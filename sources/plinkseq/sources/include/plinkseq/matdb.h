#ifndef __PLINKSEQ_MATDB_H__
#define __PLINKSEQ_MATDB_H__

#include <string>
#include <vector>

#include "sqlwrap.h"
#include "plinkseq/helper.h"
#include "plinkseq/matrix.h"


class MatDBase { 
    
 public:
    
  MatDBase() 
    { 
    }
  
  ~MatDBase()
    {
      dettach();
      sql.close();
    }
    
    
  //
  // Core functions to connect, close to database
  //
    
  bool create( const std::string & );
  bool attach( const std::string & name);
  bool init();
  bool release();
  bool dettach();

  void index();
  void drop_index();
  bool attached() { return sql.is_open(); }
  
  std::string summary(bool);
  

  //
  // Main insertion functions
  //
  
  Data::Matrix<double> read( const std::string & label ); 
  
  std::vector<std::string> names();

  bool write( const Data::Matrix<double> & m , const std::string & label , bool symmetric );
  
  void wipe( const std::string & name = "" );
     
 private:

  /* blob make_blob( const Data::Matrix<double> & m , const std::string & ); */
  /* Data::Matrix<double> read_blob( blob & m , std::string * ); */

  blob make_blob( const Data::Vector<double> & x , int );
  Data::Vector<double> read_blob( blob & x , int );


    //
    // Main datastore
    //

    SQL sql;


    //
    // modes
    //
    
    bool is_symmetric;
    
    //
    // SQL prepared queries
    //
    
    sqlite3_stmt * stmt_insert_matrix;
    sqlite3_stmt * stmt_lookup_matrix;

    sqlite3_stmt * stmt_insert_column;
    sqlite3_stmt * stmt_lookup_column;

    sqlite3_stmt * stmt_dump_matrix_names;

};



#endif

