
#include "plinkseq/matdb.h"
#include "func.h"
#include "plinkseq/matrix.h"

#include <cmath>

bool Pseq::MatDB::loader( const std::string & db , const std::string & file , const std::string & name , bool symmetric )
{

  // expect format for MAT files
  // #name
  // #row names (optional)
  // #col names (optional)
  // data... ( first row, tab delim)
  // data... ( second row, tab delim)
  
  InFile I( file );

  std::string use_name = name;

  std::string name_fromfile = I.readLine();
  if ( name == "" && name_fromfile.size() > 1 ) 
    use_name = name_fromfile.substr(1); // skip '#'

  // ignore for now...
  std::string row_names = I.readLine();  
  std::string col_names = I.readLine();  
  
  if ( name_fromfile.substr(0,1) != "#" ) Helper::halt( "problem with header format of matrix file; first three lines should start with #" );
  if ( row_names.substr(0,1) != "#" ) Helper::halt( "problem with header format of matrix file; first three lines should start with #" );
  if ( col_names.substr(0,1) != "#" ) Helper::halt( "problem with header format of matrix file; first three lines should start with #" );
  
  // first row, to get cols
  
  Data::Matrix<double> m;

  while ( ! I.eof() )
    {
      std::vector<std::string> tok = I.tokenizeLine();
      if ( tok.size() == 0  ) continue;
      std::vector<double> vals( tok.size() );
      for (int i=0;i<tok.size();i++) vals[i] = Helper::str2dbl( tok[i] );
      m.add_row( vals );
    }
  I.close();

  
  //
  // Test for a symmetric matrix
  // 

  if ( symmetric ) 
    {
      if ( m.dim1() != m.dim2() ) 
	Helper::halt( "matrix not square but --symmetric specified" );
      
      // check actual values are indeed symmetric
      const double EPS = 1e-8;
      for (int c=0;c<m.dim2();c++)
	for (int r=c+1;r<m.dim1();r++)
	  if ( fabs( m(r,c) - m(c,r) ) > EPS ) Helper::halt( "(i,j) != (j,i) elements found (at EPS>1e-8) -- matrix not symmetric" );
    }

  // 
  // Set up a MATDB instance
  //

  MatDBase matdb;  
  matdb.attach( db );
  if ( ! matdb.attached() ) Helper::halt( "problem attaching MATDB" );  
  matdb.write( m , use_name , symmetric );
  return true;

}


bool Pseq::MatDB::writer( const std::string & db , const std::string & name )
{  
  MatDBase matdb;
  matdb.attach( db );
  if ( ! matdb.attached() ) Helper::halt( "problem attaching MATDB" );

  // a list of all matrix names? 
  if ( name == "" ) 
    {
      std::vector<std::string> r = matdb.names();
      for (int i=0;i<r.size();i++) 
	std::cout << r[i] << "\n";
      return true;
    }

  // else, look for a specific matrix?
  Data::Matrix<double> m = matdb.read( name );    
  std::cout << m.dim1() << " " << m.dim2() << "\n";
  std::cout << m.print( name ) << "\n";  
  return true;
}
